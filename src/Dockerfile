FROM php:7.3-cli

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update -qq && apt-get -y upgrade

# apt packages:
RUN apt-get install -y -qq wget git ssh mariadb-client unzip

# for php extensions - not needed, all preinstalled except for gd:
#RUN apt-get install -y -qq libcurl4-openssl-dev libjpeg62-turbo-dev libpng-dev
#RUN docker-php-ext-install curl mysqli pdo_mysql gd xml simplexml json
#RUN docker-php-ext-enable curl mysqli pdo_mysql gd xml simplexml json

# Composer:
RUN EXPECTED_SIGNATURE="$(wget -q -O - https://composer.github.io/installer.sig)"; \
    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"; \
    ACTUAL_SIGNATURE="$(php -r "echo hash_file('SHA384', 'composer-setup.php');")"; \
    if [ "$EXPECTED_SIGNATURE" != "$ACTUAL_SIGNATURE" ]; \
    then \
        >&2 echo 'ERROR: Invalid installer signature'; \
        rm composer-setup.php; \
        exit 1; \
    fi; \
    php composer-setup.php --quiet && mv composer.phar /usr/local/bin/composer2 && chmod +x /usr/local/bin/composer2 && \
    php composer-setup.php --quiet --1 && mv composer.phar /usr/local/bin/composer1 && chmod +x /usr/local/bin/composer1 && \
    ln -s /usr/local/bin/composer2 /usr/local/bin/composer

# deployer:
RUN curl -LO https://deployer.org/deployer.phar && \
    mv deployer.phar /usr/local/bin/dep && \
    chmod +x /usr/local/bin/dep

# magerun2:
RUN curl -sS -O https://files.magerun.net/n98-magerun2-latest.phar && \
    curl -sS -o n98-magerun2-latest.phar.sha256 https://files.magerun.net/sha256.php?file=n98-magerun2-latest.phar && \
    shasum -a 256 -c n98-magerun2-latest.phar.sha256 && \
    mv n98-magerun2-latest.phar /usr/local/bin/magerun2 && \
    chmod +x /usr/local/bin/magerun2

# masquerade
RUN curl -sS -O https://github.com/elgentos/masquerade/releases/download/0.2.0/masquerade.phar && \
    mv masquerade.phar /usr/local/bin/masquerade && \
    chmod +x /usr/local/bin/masquerade

