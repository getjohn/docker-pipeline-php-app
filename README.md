# PHP build environment

PHP 7.3 with composer 1 & 2, deployer, and magerun2, aimed at use in CI/CD pipelines.

# Composer

`/usr/local/bin/composer1`
`/usr/local/bin/composer2`

`composer` links to `composer2`

# Deployer

Latest installed to `/usr/local/bin/dep`

# Masquerade

v0.2 installed to /usr/local/bin/masquerade

# n98-Magerun2

Latest installed to `/usr/local/bin/magerun2`

# TODO

TODO: add wp and drupal cli tools.

